<?php
function mysql_get($result, $numass=MYSQL_ASSOC) {
	$got = array();
	if(mysql_num_rows($result) == 0){return $got;}
	mysql_data_seek($result, 0);
	while ($row = mysql_fetch_array($result, $numass)) {
		array_push($got, $row);
	} 
	return $got;
}

function webs($out){
	if (preg_match_all("/https?:\/\/(www\.)?(([a-zA-Z0-9.-]+\.)+([a-zA-Z]{2,4}))(\/([a-zA-Z0-9\.\_\?\&\=\/\-\#])*)?/",$out,$match)){
		for ($p=0; $p<count($match[0]); $p++){
			if(stristr($out,'"'.$match[0][$p].'"') == FALSE){
				$out = str_replace($match[0][$p], '<a href="'.$match[0][$p].'" target="_blank">'.$match[0][$p].'</a>', $out);
			}
		}
	}
	/*debug* echo '<pre>'; print_r($match); echo '</pre>';/**/
	return $out;
}

function n2p($text){
//Fill array
$text = explode("\n", $text);

for($i=0; $i<count($text); $i++){
    //trim whitespace
    $text[$i] = trim($text[$i]);
	
	//checks wether or not the line is inside a html tag
	if(substr($text[$i], 0, 1) != '<' && substr($text[$i], -1) != '>'){
		//Place <p> tags
		if(strlen($text[$i])){
			$text[$i] = "<p>".$text[$i]."</p>\n";
		}
	}else{
		if(strlen($text[$i])){
			$text[$i] = "\n".$text[$i]."\n";
		}
	}
}

//implode array
$text = implode("", $text);
$text = webs($text);
return $text;
}

function getExtension($str) {
	$i = strrpos($str,".");
	if (!$i) { return ""; }
	$l = strlen($str) - $i;
	$ext = substr($str,$i+1,$l);
	return $ext;
}
?>
