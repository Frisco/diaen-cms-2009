<?php
function show_site_page($query){
	$res = mysql_query($query);
	$row = mysql_fetch_array($res);
	echo '<h1>'.$row['name']
	.'(<a href="edit.php?type=site&do=edit&id='.$row['id'].'">Edit</a>
	<a href="edit.php?type=site&do=update&task=delete&id='.$row['id'].'">X</a>)
	</h1>';
	if($row['custom'] == 1 && $row['islink'] == 1){
		include $row['include'];
	}else{
		echo '<div>'.n2p($row['content']).'</div>';
	}
}
echo '<div id="right">';
echo '<div id="header">site</div>';
echo '<div id="content">';

// VIEW site
if($_GET['do'] == 'view'){
	if(!isset($_GET['id'])){
		//latest site
		show_site_page("SELECT * FROM site ORDER BY position DESC limit 1");
	}else{
		//selected site
		show_site_page("SELECT * FROM site WHERE id='".$_GET['id']."'");
	}

// ADD site
}elseif($_GET['do'] == 'add'){
	$id_r = mysql_fetch_array(mysql_query("SELECT id FROM site ORDER BY id DESC limit 1"));
	$id = $id_r['id']+1;
	echo '<form method="post" action="edit.php?type=site&do=update">
		<h2>Name:</h2>
		<textarea class="head" name="name"></textarea>
		<h2>Content:</h2>
		<textarea id="body" name="body"></textarea>
		<h2>Link to file: <input type="checkbox" name="custom" value="1" /></h2>';
		//scan directory
		$directory = dir("custom");
		while ($entry = $directory->read()) {
			if(preg_match("/(\.php$)/i", $entry)){
				$filename[] = $entry;
				$filepath[] = 'custom/'.$entry;
			}
		}
		$directory->close();
		echo '<select name="include" id="select">';
		for($i=0; $i < count($filename); $i++){
			echo '<option value="'.$filepath[$i].'">'.$filename[$i].'</option>';
		}
		echo '</select>
		<h2>Hidden: <input type="checkbox" name="hidden" value="1" /></h2>
		<input id="submit" type="submit" value="submit" name="submit" />
		<input type="hidden" name="task" value="add" />
		<input type="hidden" name="id" value="'.$id.'" />
		</form>';

// EDIT site
}elseif($_GET['do'] == 'edit' && isset($_GET['id'])){
	$res = mysql_query("SELECT * FROM site WHERE id='".$_GET['id']."'");
	$row = mysql_fetch_array($res);
	//show edit form
	echo	'<form method="post" action="edit.php?type=site&do=update">
		<h2>Name:</h2>
		<textarea class="head" name="name">'.$row['name'].'</textarea>';
	if($row['custom'] == 1){
		echo
		'<h2>Content: Custom: <input type="checkbox" name="custom" value="1" checked="checked" /></h2>';
	}else{
		echo
		'<h2>Content: Custom: <input type="checkbox" name="custom" value="1" /></h2>';
	}
		echo '
		<textarea id="body" name="body">'.$row['content'].'</textarea>
		<input type="hidden" name="task" value="edit" />
		<input type="hidden" name="id" value="'.$row['id'].'" />';
//----------------------------------------------
		echo '<h2>Link to file: <input type="checkbox" name="custom" value="1" /></h2>';
		//scan directory
		$directory = dir("custom");
		while ($entry = $directory->read()) {
			if(preg_match("/(\.php$)/i", $entry)){
				$filename[] = $entry;
				$filepath[] = 'custom/'.$entry;
			}
		}
		$directory->close();
		echo '<select name="include" id="select">';
		for($i=0; $i < count($filename); $i++){
			echo '<option value="'.$filepath[$i].'">'.$filename[$i].'</option>';
		}
		echo '</select>';
//-------------------------
/*	$ress = mysql_query("SELECT * FROM category");
	while($cat = mysql_fetch_array($ress)){
		if($cat['id'] == $row['category']){
			echo '<option value="'.$cat['id'].'" selected="selected">'.$cat['name'].'</option>';
		}else{
			echo '<option value="'.$cat['id'].'">'.$cat['name'].'</option>';
		}
	}*/
//----------------------------------__**
	if($row['hidden'] == 1){		
		echo
		'<h2>Hidden: <input type="checkbox" name="hidden" value="1" checked="checked" /></h2>';
	}else{
		echo
		'<h2>Hidden: <input type="checkbox" name="hidden" value="1" /></h2>';
	}
	echo '
		<input id="submit" type="submit" value="submit" name="submit" />
	</form>';

// UPDATE site
}elseif($_GET['do'] == 'update'){

	// UPDATE ADD
	if($_POST['task'] == 'add'){
		//get new position
		$gpos = mysql_fetch_array(mysql_query("SELECT position FROM site ORDER BY position DESC LIMIT 1"));
		$position = $gpos['position'] + 1;
		if($_POST['custom'] != 1){
			$custom = 0;
		}else{
			$custom = 1;
		}
		if($_POST['hidden'] != 1){
			$hidden = 0;
		}else{
			$hidden = 1;
		}

		//update database
		mysql_query("INSERT INTO site (id, name, position, content, custom, hidden, include)
		VALUES ('".$_POST['id']."', '".$_POST['name']."', '".$position."', '".$_POST['body']."', '".$custom."', '".$hidden."', '".$_POST['include']."')");
		//show added page
		show_site_page("SELECT * FROM site WHERE id='".$_POST['id']."'");

	// UPDATE EDIT
	}elseif($_POST['task'] == 'edit'){
		if(!$_POST['islink']){
			$islink = 0;
		}else{
			$islink = 1;
		}
		if(!$_POST['hidden']){
			$hidden = 0;
		}else{
			$hidden = 1;
		}
		if(!$_POST['custom']){
			$custom = 0;
		}else{
			$custom = 1;
		}
		//update database
mysql_query("UPDATE site SET name = '".$_POST['name']."', content = '".$_POST['body']."', islink = '".$islink ."', custom = '".$custom."', hidden = '".$hidden."', include = '".$_POST['include']."' WHERE id = '".$_POST['id']."'");
		//show edited page
		show_site_page("SELECT * FROM site WHERE id='".$_POST['id']."'");

	// UPDATE DELETION
	}elseif($_GET['task'] == 'delete'){
		if(isset($_GET['true'])){
			//delete
			mysql_query("DELETE FROM site WHERE id = '".$_GET['id']."'");
			//show latest page
			show_site_page("SELECT * FROM site ORDER BY id DESC limit 1");
		}else{
			$get = mysql_fetch_array(mysql_query("SELECT name FROM site WHERE id='".$_GET['id']."'"));
			//deletion confirmation
			echo '<div id="msg">
			Are you sure you want to delete "'.$get['name'].'"?<br />
			<a href="edit.php?type=site&do=update&task=delete&id='.$_GET['id'].'&true=1">Yes</a> 
			<a href="edit.php?type=site&do=view&id='.$_GET['id'].'">No</a>
			</div>';
		}
	}
}elseif($_GET['do'] == 'pos'){
	include 'sort.php';
}
echo '</div>';

// site NAVIGATION LIST
		echo '<div id="map"><ul>
		<li class="b">site</li>';
		$res = mysql_query("SELECT * FROM site ORDER BY position ASC");
		while($row = mysql_fetch_array($res)){
			if($row['islink']){
				echo '<li>'.$row['name'].'<br />(<a href="edit.php?type=site&do=edit&id='.$row['id'].'">Edit</a>			
				<a href="edit.php?type=site&do=update&task=delete&id='.$row['id'].'">X</a>)
				</li>';
			}else{
				echo '<li>'.$row['name'].'<br />(<a href="edit.php?type=site&do=view&id='.$row['id'].'">View</a>
				<a href="edit.php?type=site&do=edit&id='.$row['id'].'">Edit</a>			
				<a href="edit.php?type=site&do=update&task=delete&id='.$row['id'].'">X</a>)
				</li>';
			}
		}
		echo '</ul></div>';
?>
