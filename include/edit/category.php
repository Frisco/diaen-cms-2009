<?php
	/***********************************
			     CATEGORY
	***********************************/
function show_category_page($query){
	$res = mysql_query($query);
	$row = mysql_fetch_array($res);
	$site = mysql_fetch_array(mysql_query("SELECT name FROM site WHERE id='".$row['site']."'"));
	$article = mysql_fetch_array(mysql_query("SELECT header, content FROM article WHERE id='".$row['article']."'")); 
	echo '<h1>'.$row['name']
	.'(<a href="edit.php?type=category&do=edit&id='.$row['id'].'">Edit</a>
	<a href="edit.php?type=category&do=update&task=delete&id='.$row['id'].'">X</a>)
	</h1>
	<strong>This category is shown in '.$site['name'].'</strong>';
	if($row['islink'] == 1){
		echo
		'<h2>Article content:</h2>
		<h1>'.$article['header'].'</h1>
		<div>'.n2p($article['content']).'</div>';
	}else{
		echo '<br /><strong> This category is not linked to an article </strong>';
	}
}
echo '<div id="right">';
echo '<div id="header">Category</div>';
echo '<div id="content">';

// VIEW CATEGORY
if($_GET['do'] == 'view'){
	if(!isset($_GET['id'])){
		//latest article
		show_category_page("SELECT * FROM category ORDER BY id DESC limit 1");
	}else{
		//selected article
		show_category_page("SELECT * FROM category WHERE id='".$_GET['id']."'");
	}

// ADD CATEGORY
}elseif($_GET['do'] == 'add'){
	$id_r = mysql_fetch_array(mysql_query("SELECT id FROM category ORDER BY id DESC limit 1"));
	$id = $id_r['id']+1;
	echo '<form method="post" action="edit.php?type=category&do=update">
		<h2>Name:</h2>
			<textarea class="head" name="name"></textarea>
		<h2>Link to: <input type="checkbox" name="islink" value="1" /></h2>
		<select name="article" id="select">';
		$art = mysql_query("SELECT * FROM article");
		while($artl = mysql_fetch_array($art)){
			echo '<option value="'.$artl['id'].'">'.$artl['name'].'</option>';
		}
	echo '</select>
		<h2>Show on site:</h2>
			<select name="site" id="select">';
		$ress = mysql_query("SELECT * FROM site");
		while($site = mysql_fetch_array($ress)){
			echo '<option value="'.$site['id'].'">'.$site['name'].'</option>';
		}
		echo '</select>
		<input id="submit" type="submit" value="submit" name="submit" />
		<input type="hidden" name="task" value="add" />
		<input type="hidden" name="id" value="'.$id.'" />
	</form>';

// EDIT CATEGORY
}elseif($_GET['do'] == 'edit' && isset($_GET['id'])){
	$res = mysql_query("SELECT * FROM category WHERE id='".$_GET['id']."'");
	$row = mysql_fetch_array($res);
	//show edit form
	echo '<form method="post" action="edit.php?type=category&do=update">
		<h2>Name:</h2>
			<textarea class="head" name="name">'.$row['name'].'</textarea>';
	if($row['islink'] == 1){		
		echo '<h2>Link to: <input type="checkbox" name="islink" value="1" checked="checked" /></h2>';
	}else{
		echo '<h2>Link to: <input type="checkbox" name="islink" value="1" /></h2>';
	}		
			echo '<select name="article" id="select">';
		$art = mysql_query("SELECT * FROM article");
		while($artl = mysql_fetch_array($art)){
			if($artl['id'] == $row['article']){
				echo '<option value="'.$artl['id'].'" selected="selected">'.$artl['name'].'</option>';
			}else{
				echo '<option value="'.$artl['id'].'">'.$artl['name'].'</option>';
			}
		}
		
	echo '</select>
		<h2>Show on site:</h2>
			<select name="site" id="select">';
		$ress = mysql_query("SELECT * FROM site");
		while($site = mysql_fetch_array($ress)){
			if($site['id'] == $row['site']){
				echo '<option value="'.$site['id'].'" selected="selected">'.$site['name'].'</option>';
			}else{
				echo '<option value="'.$site['id'].'">'.$site['name'].'</option>';
			}
		}
		echo '</select>
		<input id="submit" type="submit" value="submit" name="submit" />
		<input type="hidden" name="task" value="edit" />
		<input type="hidden" name="id" value="'.$row['id'].'" />
	</form>';

// UPDATE CATEGORY
}elseif($_GET['do'] == 'update'){

	// UPDATE ADD
	if($_POST['task'] == 'add'){
		//get new position
		$gpos = mysql_fetch_array(mysql_query("SELECT position FROM category WHERE site = '".$_POST['site']."' ORDER BY position DESC"));
		$position = $gpos['position'] + 1;
		if($_POST['islink'] != 1){
			$islink = 0;
			$article = 0;
		}else{
			$islink = 1;
			$article = $_POST['article'];
		}

		//update database
		mysql_query("INSERT INTO category (id, name, islink, article, site, position)
		VALUES ('".$_POST['id']."', '".$_POST['name']."', '".$islink."', '".$article."', '".$_POST['site']."', '".$position."')");

		//show added page
		show_category_page("SELECT * FROM category WHERE id='".$_POST['id']."'");

	// UPDATE EDIT
	}elseif($_POST['task'] == 'edit'){
		if($_POST['islink'] != 1){
			$islink = 0;
			$article = 0;
		}else{
			$islink = 1;
			$article = $_POST['article'];
		}
		//update database
		mysql_query("UPDATE category 
		SET name = '".$_POST['name']."', islink = '".$_POST['islink']."', article = '".$article."', site = '".$_POST['site']."'
		WHERE id = '".$_POST['id']."'");
		//show edited page
		show_category_page("SELECT * FROM category WHERE id='".$_POST['id']."'");

	// UPDATE DELETION
	}elseif($_GET['task'] == 'delete'){
		if(isset($_GET['true'])){
			//delete
			mysql_query("DELETE FROM category WHERE id = '".$_GET['id']."'");
			//show latest page
			show_category_page("SELECT * FROM category ORDER BY id DESC limit 1");
		}else{
			$get = mysql_fetch_array(mysql_query("SELECT name FROM category WHERE id='".$_GET['id']."'"));
			//deletion confirmation
			echo '<div id="msg">
			Are you sure you want to delete "'.$get['name'].'"?<br />
			<a href="edit.php?type=category&do=update&task=delete&id='.$_GET['id'].'&true=1">Yes</a> 
			<a href="edit.php?type=category&do=view&id='.$_GET['id'].'">No</a>
			</div>';
		}
	}
}elseif($_GET['do'] == 'pos'){
	include 'sort.php';
}
echo '</div>';
// CATEGORY NAVIGATION LIST
echo '<div id="map"><ul>
<li class="b">Category</li>';
$res = mysql_query("SELECT * FROM site ORDER BY position");
$res2 = mysql_query("SELECT * FROM category ORDER BY position");
$art = mysql_get($res2);
while($cat = mysql_fetch_array($res)){
	if($cat['islink'] == 1){
		echo '<li class="cat"><strong>'.$cat['name'].'</strong><br />(<a href="edit.php?type=site&do=edit&id='.$cat['id'].'">Edit</a>
			<a href="edit.php?type=site&do=view&id='.$cat['id'].'">View</a>
			<a href="edit.php?type=site&do=update&task=delete&id='.$cat['id'].'">X</a>)</li>';		
	}else{
		echo '<li class="cat"><strong>'.$cat['name'].'</strong></li>';
	}
		for($i=0; $i<count($art); $i++){
			if($art[$i]['site'] == $cat['id']){
			echo '<li>'.$art[$i]['name'].'<br />(<a href="edit.php?type=category&do=view&id='.$art[$i]['id'].'">View</a>
			<a href="edit.php?type=category&do=edit&id='.$art[$i]['id'].'">Edit</a>			
			<a href="edit.php?type=category&do=update&task=delete&id='.$art[$i]['id'].'">X</a>)
			</li>';
		}
	}
}
echo '</ul></div>';

?>
