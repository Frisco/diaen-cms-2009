<?php
function show_article_page($query){
	$res = mysql_query($query);
	$row = mysql_fetch_array($res);
	echo '<h1>
	'.$row['header'].'
	(<a href="edit.php?type=article&do=edit&id='.$row['id'].'">Edit</a>
	<a href="edit.php?type=article&do=update&task=delete&id='.$row['id'].'">X</a>)
	</h1>
	<div>'.n2p($row['content']).'</div>';
}
echo '<div id="right">';
echo '<div id="header">Article</div>';
echo '<div id="content">';

// VIEW ARTICLE
if($_GET['do'] == 'view'){
	if(!isset($_GET['id'])){
		//latest article
		show_article_page("SELECT * FROM article ORDER BY id DESC limit 1");
	}else{
		//selected article
		show_article_page("SELECT * FROM article WHERE id='".$_GET['id']."'");
	}

// ADD ARTICLE
}elseif($_GET['do'] == 'add'){
		$id_r = mysql_fetch_array(mysql_query("SELECT id FROM article ORDER BY id DESC limit 1"));
		$id = $id_r['id']+1;
		echo '<form method="post" action="edit.php?type=article&do=update" enctype="multipart/form-data">
			<h2>Link name:</h2>
				<textarea class="head" name="name"></textarea>
			<h2>Header:</h2>
				<textarea class="head" name="head"></textarea>
			<h2>Content:</h2>
				<textarea id="body" name="body"></textarea>
			<h2>Category:</h2>
				<select name="category" id="select">
				<option value="-1">None</option>';
			$ress = mysql_query("SELECT * FROM category");
			while($cat = mysql_fetch_array($ress)){
				echo '<option value="'.$cat['id'].'">'.$cat['name'].'</option>';
			}
			echo '</select>
			<input id="submit" type="submit" value="submit" name="submit" />
			<input type="hidden" name="task" value="add" />
			<input type="hidden" name="id" value="'.$id.'" />
		</form>';

// EDIT ARTICLE
}elseif($_GET['do'] == 'edit' && isset($_GET['id'])){
	$res = mysql_query("SELECT * FROM article WHERE id='".$_GET['id']."'");
	$row = mysql_fetch_array($res);
	//show edit form
	echo '<form method="post" action="edit.php?type=article&do=update" enctype="multipart/form-data">
		<h2>Link name:</h2>
			<textarea class="head" name="name">'.$row['name'].'</textarea>
		<h2>Header:</h2>
			<textarea class="head" name="head">'.$row['header'].'</textarea>
		<h2>Content:</h2>
			<textarea id="body" name="body">'.$row['content'].'</textarea>
		<h2>Category:</h2>
			<select name="category" id="select">
			<option value="-1">None</option>';
		$ress = mysql_query("SELECT * FROM category");
		while($cat = mysql_fetch_array($ress)){
			if($cat['id'] == $row['category']){
				echo '<option value="'.$cat['id'].'" selected="selected">'.$cat['name'].'</option>';
			}else{
				echo '<option value="'.$cat['id'].'">'.$cat['name'].'</option>';
			}
		}
		echo '</select>
		<input id="submit" type="submit" value="submit" name="submit" />
		<input type="hidden" name="task" value="edit" />
		<input type="hidden" name="id" value="'.$row['id'].'" />
	</form>';

// UPDATE ARTICLE
}elseif($_GET['do'] == 'update'){

	// UPDATE ADD
	if($_POST['task'] == 'add'){
		//get new position
		$gpos = mysql_fetch_array(mysql_query("SELECT position FROM article WHERE category = '".$_POST['category']."' ORDER BY position DESC"));
		$position = $gpos['position'] + 1;
		//update database
		mysql_query("INSERT INTO article (id, name, header, content, category, position) VALUES ('".$_POST['id']."', '".$_POST['name']."',
		'".$_POST['head']."', '".$_POST['body']."', '".$_POST['category']."', '".$position."')");
		//show new page
		show_article_page("SELECT * FROM article ORDER BY id DESC limit 1");

	// UPDATE EDIT
	}elseif($_POST['task'] == 'edit'){
		//update database
		mysql_query("UPDATE article 
		SET name = '".$_POST['name']."', header = '".$_POST['head']."', content = '".$_POST['body']."', category = '".$_POST['category']."'
		WHERE id = '".$_POST['id']."'");
		//show edited page
		show_article_page("SELECT * FROM article WHERE id='".$_POST['id']."'");

	// UPDATE DELETION
	}elseif($_GET['task'] == 'delete'){
		if(isset($_GET['true'])){
			//delete
			mysql_query("DELETE FROM article WHERE id = '".$_GET['id']."'");
			//show latest page
			show_article_page("SELECT * FROM article ORDER BY id DESC limit 1");
		}else{
			$get = mysql_fetch_array(mysql_query("SELECT header FROM article WHERE id='".$_GET['id']."'"));
			//deletion confirmation
			echo '<div id="msg">
			Are you sure you want to delete "'.$get['header'].'"?<br />
			<a href="edit.php?type=article&do=update&task=delete&id='.$_GET['id'].'&true=1">Yes</a> 
			<a href="edit.php?type=article&do=view&id='.$_GET['id'].'">No</a>
			</div>';
		}
	}
}elseif($_GET['do'] == 'pos'){
	include 'sort.php';
}

// ARTICLE NAVIGATION LIST
echo '</div>';
echo '<div id="map"><ul>
<li class="b">Article</li>';
$res = mysql_query("SELECT * FROM category ORDER BY name");
$res2 = mysql_query("SELECT * FROM article ORDER BY position");
$art = mysql_get($res2);
while($cat = mysql_fetch_array($res)){
	if($cat['islink'] == 1){
		echo '<li class="cat"><strong>'.$cat['name'].'</strong><br />(<a href="edit.php?type=article&do=edit&id='.$cat['article'].'">Edit</a>
			<a href="edit.php?type=article&do=view&id='.$cat['article'].'">View</a>
			<a href="edit.php?type=article&do=update&task=delete&id='.$cat['article'].'">X</a>)</li>';		
	}else{
		echo '<li class="cat"><strong>'.$cat['name'].'</strong></li>';
	}
		for($i=0; $i<count($art); $i++){
			if($art[$i]['category'] == $cat['id']){
			echo '<li>'.$art[$i]['name'].'<br />(<a href="edit.php?type=article&do=view&id='.$art[$i]['id'].'">View</a>
			<a href="edit.php?type=article&do=edit&id='.$art[$i]['id'].'">Edit</a>
			<a href="edit.php?type=article&do=update&task=delete&id='.$art[$i]['id'].'">X</a>)
			</li>';
		}
	}
}
echo '</ul></div>';
?>
