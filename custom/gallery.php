<?php
$d = dir("./bilder");
//scan directory
while($entry = $d->read()) {
	//allow extensions
	if(
	preg_match("/(\.gif$)/i", $entry) ||
	preg_match("/(\.bmp$)/i", $entry)    ||
	preg_match("/(\.png$)/i", $entry)    ||
	preg_match("/(\.jpeg$)/i", $entry)   ||
	preg_match("/(\.jpg$)/i", $entry)){
		$pics[] = $entry;
	}
}
$d->close();

//count pictures
$numPics = count($pics);

//What picture to show
$thispic = 0;
if(isset($_GET['id'])){
	$thispic = $_GET['id'];
}

//img tag for current image
$getsize = getimagesize('bilder/'.$pics[$thispic]);
if($getsize[0]>860){
	$width=860;
}else{
	$width=$getsize[0];
}
$thisImage = '<img src="bilder/'.$pics[$thispic].'" alt="'.$pics[$thispic].'" width="'.$width.'" />';

//prev button
$prevurl = 'Previous';
if($thispic > 0){
	$prev = $thispic - 1;
	$prevurl = '<a href="'.$_SERVER['PHP_SELF'].'?site=7&id='.$prev.'">Previous</a>';
}

//next button
$nexturl = 'Next';
if($thispic < ($numPics - 1)){
	$next = $thispic + 1;
	$nexturl = '<a href="'.$_SERVER['PHP_SELF'].'?site=7&id='.$next.'">Next</a>';
}

//picture array
$urlarray = '';
for ($i=0;$i<count($pics);$i++){
	$j = $i + 1;
	if($i != $thispic){
		$urlarray .= '<a href="'.$_SERVER['PHP_SELF'].'?site=7&id='.$i.'">'.$j.'</a>';
	}else{
		//highlight
		$urlarray .= ' <span class="high">' . $j . '</span> ';
	}
}
$no_nav = TRUE;
?>
<div id="gallery">
	<div id="num_lnk"><?=$urlarray?></div>
	<div id="nav"><?=$prevurl?> <?=$nexturl?></div>
	<div id="image"><a href="<?php echo $_SERVER['PHP_SELF'].'?site=7&id='.$next;?>"><?=$thisImage?></a></div>
</div>
<br />
