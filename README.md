# Dian - php CMS #

CMS developed as a tool for a website for Centre of Reproduction and Reproductive Toxicology (CRRT), a research group at the Norwegian School of Veterinary Science. Its purpose is to showcase their research, publications and group members. 


I'm making this CMS available, since the website no longer exists. 


Made in 2009 and was my first paid job in web development as a freelancer. 
Prior to this I never made anything that had an admin page and was meant for content creation.