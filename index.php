<?php
include 'include/DB.php';
include 'include/functions.php';

//variables
$no_nav = FALSE;

//get site contents
$site_r = mysql_query("SELECT * FROM site WHERE position >= 0 ORDER BY position");
$site = mysql_get($site_r);

//default site
if(!isset($_GET['site'])){
	$_GET['site'] = 0;
}

//html header
echo
'<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en-US" xml:lang="en-US" xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<link rel="stylesheet" type="text/css" href="/css/style_new_900.css" />
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
<link rel="icon" href="favicon.ico" type="image/x-icon">
<!--[if IE]>
    <link rel="stylesheet" type="text/css" href="/css/IE.css" />
<![endif]-->
<title>Centre of Reproduction and Reproductive Toxicology</title>
</head>
<body>
<a name="top" id="top"></a>
<div id="main">
<div id="header">
<p>Centre of Reproduction and Reproductive Toxicology</p>
</div>
<div id="body">';

//show navigation
echo '<div id="nav_hor"><ul>';
for($l=0; $l<mysql_num_rows($site_r); $l++){
	if($site[$l]['position'] == $_GET['site']){
		echo '<li><a href="/'.$site[$l]['position'].'" id="active">'.$site[$l]['name'].'</a></li>';
	}elseif(!$site[$l]['hidden']){
		echo '<li><a href="/'.$site[$l]['position'].'">'.$site[$l]['name'].'</a></li>';
	}
}
echo '</ul></div>';
echo "\n";
if(isset($_GET['site']) && !empty($site[$_GET['site']]['include'])){
	include $site[$_GET['site']]['include'];
	$no_nav = TRUE;

//show article content
}elseif(isset($_GET['art'])){
	$art_r = mysql_query("SELECT * FROM article where id='".mysql_real_escape_string($_GET['art'])."'");
	$art = mysql_get($art_r);
	echo '<div id="container_1">
		<h1>'.$art[0]['header'].'</h1>'.n2p($art[0]['content']).'
	</div>';

//show site content
}else{
	//show custom site content
	if($site[$_GET['site']]['custom'] == 1){
		echo n2p($site[$_GET['site']]['content']);
		$no_nav = TRUE;

	//show site content
	}else{
		echo '<div id="container_1">
		'.n2p($site[$_GET['site']]['content']).'
		</div>';
	}
}

//vertical navigation
if(!$no_nav){
	//get category links
	$cat_r = mysql_query("SELECT * FROM category WHERE site='".mysql_real_escape_string($_GET['site'])."' ORDER BY position");
	$cat = mysql_get($cat_r);

	//generate list
	echo'<div id="container_2"><div id="nav_ver"><ul>';
	for($l=0; $l<mysql_num_rows($cat_r); $l++){
		//get article links
		$art_r = mysql_query("SELECT * FROM article WHERE category='".mysql_real_escape_string($cat[$l]['id'])."' ORDER BY position");
		$art = mysql_get($art_r);
		if($cat[$l]['islink'] == 1){
			echo '<li class="cat_l"><a href="/'.$_GET['site'].'/'.$cat[$l]['article'].'">'.$cat[$l]['name'].'</a></li>';
		}else{
			echo '<li class="cat">'.$cat[$l]['name'].'</li>';
		}
		for($r=0; $r<mysql_num_rows($art_r); $r++){
			if(isset($_GET['art']) && $_GET['art'] == $art[$r]['id']){
				echo '<li class="art_a">
				<a href="/'.$_GET['site'].'/'.$art[$r]['id'].'">'.$art[$r]['name'].'</a>
				</li>';
			}else{
				echo '<li class="art"><a href="/'.$_GET['site'].'/'.$art[$r]['id'].'">'.$art[$r]['name'].'</a></li>';
			}
		}
	}
	echo'</ul></div></div>';
}

//disconnect
mysql_close();

//html end
echo '</div></div><img src="/gfx/NVH_logo_english_small.gif" id="NVH" /></body></html>';
?>
